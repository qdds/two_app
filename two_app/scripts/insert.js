/**
 * Created by David on 2017/2/6.
 * 版权所有：广州聆歌信息科技有限公司
 * Legle co,.ltd.
 */

var user = require('../services/user');

var testDate = require('./testdata');

user.create(testDate.user1)
    .then(function () {
        return user.create(testDate.user2);
    })
    .catch(function (err) {
        console.log(JSON.stringify(err));
        console.log("创建测试数据出错：" + err.message);
    })