/**
 * Created by David on 2017/3/7.
 * 版权所有：广州聆歌信息科技有限公司
 * Legle co,.ltd.
 */
module.exports = {
    product_1: {
        productid: 'product_1_init'
        , name: '背部舒缓嫩肌初体验'
        , preview_picture: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/6-FENGMIAN.jpg"]}'
        , describe: '<p><img src="http://omg8evjaw.bkt.clouddn.com/6-1.jpg" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/6-2.jpg" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/6-3.jpg" style="max-width: 100%;"></p>'
        , retail_price: 65
        , unit: '次'
    }

    , product_2: {
        productid: 'product_2_init'
        , name: '微雕半永久纹绣私人定制'
        , preview_picture: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/2-FENGMIAN.jpg"]}'
        , describe: '<p><img src="http://omg8evjaw.bkt.clouddn.com/1-1.jpg" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/2-1.png" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/2-2.png" style="max-width: 100%;"></p>'
        , retail_price: 856
        , unit: '次'
    }

    , product_3: {
        productid: 'product_3_init'
        , name: '无痕面部痣祛除'
        , preview_picture: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/3-FENGMIAN.jpg"]}'
        , describe: '<p><img src="http://omg8evjaw.bkt.clouddn.com/3-1.jpg" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/3-2.jpg" style="max-width: 100%;"></p>'
        , retail_price: 58
        , unit: '次'
    }

    , shelves_1: {
        shelvesid: 'shelves_1_init'
        ,
        shelves_type: 1
        ,
        preview_picture: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/6-FENGMIAN.jpg"]}'
        ,
        title: '背部舒缓嫩肌初体验'
        ,
        describe: '<p><img src="http://omg8evjaw.bkt.clouddn.com/6-1.jpg" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/6-2.jpg" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/6-3.jpg" style="max-width: 100%;"></p>'
        ,
        label: '{"text": ["火热促销"]}'
        ,
        total_price: 65
        ,
        selling_price: 38
        ,
        amount: 50
        ,
        begin_datetime: '2017-02-06 14:30:30'
        ,
        finish_datetime: '2017-12-06 14:30:30'
        ,
        status: 1
        ,
        isHomeShow: 0
        ,
        oneUserBuy: 0
    }

    , shelves_2: {
        shelvesid: 'shelves_2_init'
        ,
        shelves_type: 2
        ,
        preview_picture: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/3-FENGMIAN.jpg","http://omg8evjaw.bkt.clouddn.com/6-FENGMIAN.jpg"]}'
        ,
        title: '背部舒缓嫩肌初体验+无痕面部痣祛除'
        ,
        describe: '<p><img src="http://omg8evjaw.bkt.clouddn.com/3-1.jpg" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/3-2.jpg" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/6-1.jpg" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/6-2.jpg" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/6-3.jpg" style="max-width: 100%;"></p>'
        ,
        label: '{"text": ["特色套餐"]}'
        ,
        total_price: 123
        ,
        selling_price: 98
        ,
        amount: 50
        ,
        begin_datetime: '2017-02-06 14:30:30'
        ,
        finish_datetime: '2017-12-06 14:30:30'
        ,
        status: 1
        ,
        isHomeShow: 0
        ,
        oneUserBuy: 0
    }

    , shelves_3: {
        shelvesid: 'shelves_3_init'
        ,
        shelves_type: 4
        ,
        preview_picture: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/2-FENGMIAN.jpg"]}'
        ,
        title: '微雕半永久纹绣私人定制'
        ,
        describe: '<p><img src="http://omg8evjaw.bkt.clouddn.com/2-1.png" style="max-width: 100%;">' +
        '<img src="http://omg8evjaw.bkt.clouddn.com/2-2.png" style="max-width: 100%;"></p>'
        ,
        label: '{"text": ["限时秒杀"]}'
        ,
        total_price: 856
        ,
        selling_price: 688
        ,
        amount: 50
        ,
        begin_datetime: '2017-03-12 0:00:00'
        ,
        finish_datetime: '2017-03-12 23:59:59'
        ,
        status: 1
        ,
        isHomeShow: 1
        ,
        oneUserBuy: 1
    }

    , activity_1: {
        activityid: 'activity_1_init'
        ,
        title: '紧致抗皱水光针，密集补水滋润'
        ,
        preview_picture: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/1-FENGMIAN.jpg"]}'
        ,
        describe: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/1-1.jpg","http://omg8evjaw.bkt.clouddn.com/1-2.jpg",' +
        '"http://omg8evjaw.bkt.clouddn.com/1-3.jpg","http://omg8evjaw.bkt.clouddn.com/1-4.jpg","http://omg8evjaw.bkt.clouddn.com/1-5.jpg",' +
        '"http://omg8evjaw.bkt.clouddn.com/1-6.jpg","http://omg8evjaw.bkt.clouddn.com/1-7.jpg"]}'
        ,
        rule: "还你肌肤水嫩，充盈紧实",
        bengin_datetime: '2017-03-09 14:00:00'
        ,
        finish_datetime: '2017-12-06 14:00:00'
    }
    , activity_2: {
        activityid: 'activity_2_init'
        ,
        title: '冰点脱毛，享零瑕疵美肌'
        ,
        preview_picture: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/4-FENGMIAN.jpg"]}'
        ,
        describe: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/4-1.jpg","http://omg8evjaw.bkt.clouddn.com/4-2.jpg","http://omg8evjaw.bkt.clouddn.com/4-3.jpg"]}'
        ,
        rule: "还你肌肤水嫩，充盈紧实",
        bengin_datetime: '2017-03-09 14:00:00'
        ,
        finish_datetime: '2017-12-06 14:00:00'
    }
    , activity_3: {
        activityid: 'activity_3_init'
        ,
        title: 'La Colline皇牌眼部护理限时特惠仅需135'
        ,
        preview_picture: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/5-FENGMIAN.jpg"]}'
        ,
        describe: '{"pictures": ["http://omg8evjaw.bkt.clouddn.com/5-1.jpg","http://omg8evjaw.bkt.clouddn.com/5-2.jpg",' +
        '"http://omg8evjaw.bkt.clouddn.com/5-3.jpg","http://omg8evjaw.bkt.clouddn.com/5-4.jpg","http://omg8evjaw.bkt.clouddn.com/5-5.jpg"]}'
        ,
        rule: "呵护眼周，水润清透，淡化岁月痕迹",
        bengin_datetime: '2017-03-09 14:00:00'
        ,
        finish_datetime: '2017-12-06 14:00:00'
    }

    , beautician_1: {
        beauticianid: 'beautician_1_init'
        , real_name: '李老师'
        , headshot: 'http://img2.imgtn.bdimg.com/it/u=2356544990,4236143590&fm=23&gp=0.jpg'
        , mobile: '13312345678'
        , label: '{"label": ["五星美容师", "面部美容"]}'
        , brief: "主要从事面部美容，技术高超，妙手回春，获得五星美容师称号。"
    }
}