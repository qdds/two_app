/**
 * Created by Wonderchief on 2017/1/7.
 * 版权所有：广州聆歌信息科技有限公司
 * Legle co,.ltd.
 */
var ejs = require('ejs');
var models = require('../models').sequelize.models;
var fs = require('fs');
var path = require('path');

function adater(model) {
    var _m = {
        modelName: model.name
        , indexes: []
        , unique: []
        , fields: []
        , type: []  //类型有UUID TEXT STRING DATE FLOAT DATETIME INTEGER ...等 具体看sequelize的定义,字符串
        , comments: []
        , attributes: model.attributes
    };
    _m.indexes = model.options.indexes;
    for (var fieldName in model.attributes) {
        // console.log(fieldName);
        // console.log("1,"+model.attributes[fieldName].type);
        // console.log("2,"+model.attributes[fieldName].type.key);
        // console.log("3,"+model.attributes[fieldName].type.toSql());
        // console.log("4,"+model.attributes[fieldName].type.toString());
        if (model.attributes[fieldName].primaryKey) {
            _m.primaryKey = fieldName;
        }
        if (model.attributes[fieldName].unique) {
            _m.unique.push(fieldName);
        }
        if (model.attributes[fieldName].references != null) {
            var indexobj = {
                name: fieldName,
                fields: [fieldName]
            };
            _m.indexes.push(indexobj);
        }
        _m.fields.push(fieldName);
        _m.type.push(model.attributes[fieldName].type.key);
        _m.comments.push(model.attributes[fieldName].comment != null ? model.attributes[fieldName].comment : "");
    }

    return _m;
}
function generate() {
    var service_temp = fs.readFileSync(__dirname + '/temp/service_temp.ejs', 'utf8');
    var service_base_temp = fs.readFileSync(__dirname + '/temp/service_base_temp.ejs', 'utf8');
    var route_temp = fs.readFileSync(__dirname + '/temp/route_temp.ejs', 'utf8');
    var script_crud_temp = fs.readFileSync(__dirname + '/temp/script_crud_temp.ejs', 'utf8');
    // var admin_html_temp = fs.readFileSync(__dirname + '/temp/admin_html_temp.ejs', 'utf8');

    if (!fs.existsSync(__dirname + "/output/")) {
        fs.mkdirSync(__dirname + "/output/");
    }
    if (!fs.existsSync(__dirname + "/output/services/")) {
        fs.mkdirSync(__dirname + "/output/services/");
    }
    if (!fs.existsSync(__dirname + "/output/services_base/")) {
        fs.mkdirSync(__dirname + "/output/services_base/");
    }
    if (!fs.existsSync(__dirname + "/output/script_crud/")) {
        fs.mkdirSync(__dirname + "/output/script_crud/");
    }
    for (var model in models) {
        var _obj = adater(models[model]);

        // console.log(model);

        var _file_service_base = ejs.render(service_base_temp, _obj);
        var _file_service = ejs.render(service_temp, _obj);
        var _script_crud = ejs.render(script_crud_temp, _obj);

        fs.writeFileSync(__dirname + "/output/services_base/" + model + "_base.js", _file_service_base, {encoding: 'utf8'});
        fs.writeFileSync(__dirname + "/output/services/" + model + ".js", _file_service, {encoding: 'utf8'});
        fs.writeFileSync(__dirname + "/output/script_crud/" + model + ".js", _script_crud, {encoding: 'utf8'});

        if (!fs.existsSync(path.join(__dirname, '../routes/') + model + ".js")) {
            var _file_route = ejs.render(route_temp, _obj);
            fs.writeFileSync(path.join(__dirname, '../routes/') + model + ".js", _file_route, {encoding: 'utf8'});
        }
        // console.log(models[model].attributes);
        // break;
        //console.log("generate service file " + model + "_base.js");
        //console.log(_file);
        //console.log(models[model]);
        //console.log(model.attributes[fieldName].type);
    }
}
generate();
