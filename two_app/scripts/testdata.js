/**
 * Created by David on 2017/2/6.
 * 版权所有：广州聆歌信息科技有限公司
 * Legle co,.ltd.
 */

var service = require('../services/service');

module.exports = {
    user1:{
        phone:'18826252271',
        pass:'87335b6be9481968be2cf3e4b77c3f1d587bcbd566f4ce3cbd56e304989f30e0',
        title:'莫建鹏',
        userid:'mjp1',
        is_phone:1
    },
    user2:{
        email:'18826252271@163.com',
        pass:'87335b6be9481968be2cf3e4b77c3f1d587bcbd566f4ce3cbd56e304989f30e0',
        title:'李开心',
        userid:'mjp2',
        is_phone:1
    },

    service_1: {
        serviceid: service.encrypt('baseservice_30'),
        service_name: '基础服务',
        retail_price: 688,
        real_price: 0.01,
        charge_cycle: 30,
        type: 1
    },

    service_2: {
        serviceid: service.encrypt('baseservice_tail_90'),
        service_name: '基础服务-试用',
        retail_price: 0,
        real_price: 0,
        charge_cycle: 90,
        type: 2
    }

};