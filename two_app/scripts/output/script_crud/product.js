/**
* Created by Wonderchief on 2017/1/15.
* This is a auto-generated code file.
* 版权所有：广州聆歌信息科技有限公司
*/
function product(option){

    var obj={};
    obj.list=[];
    obj.single=null;
    obj.fields=['productid','name','preview_picture','describe','retail_price','unit','category','total_sales','total_sales_volume','praise_list','postage','customerid','createdAt','updatedAt',];
    obj.type=['UUID','STRING','TEXT','TEXT','FLOAT','STRING','INTEGER','FLOAT','INTEGER','TEXT','FLOAT','UUID','DATE','DATE',];
    obj.comments=[ '产品ID', '产品名称', '产品预览图（最多5张）', '产品图文描述', '产品建议零售价', '产品单位', '品类', '累计销售额', '累积销量', '点赞列表', '邮费', '客户ID','创建时间','更新时间',];
    obj.count=0;
    obj.offset = 0;
    obj.size = 20;
    obj.page = 1;
    obj.container=option.container || "";
    var URL = function () {
        this.url = option.url != null ? option.url : "../product";
        return this.url;
    };
    URL.prototype.toString=function () {
        return this.url;
    };
    URL.prototype.toList=function () {
        return this.url+"/listPage";
    };
    URL.prototype.toCreate=function () {
        return this.url+"/create";
    };
    URL.prototype.toUpdate=function () {
        return this.url+"/update";
    };
    URL.prototype.toDelete=function () {
        return this.url+"/delete";
    };
    URL.prototype.toFind=function () {
        return this.url+"/find";
    };
    obj.url=new URL();

    obj.nextPage=function(fn){
        obj.offset+=obj.size;
        if(fn!=null)
            fn();
    };
    obj.lastPage=function(fn){
        if(obj.offset-obj.size<0)
            obj.offset=0;
        else
            obj.offset-=obj.size;
        if(fn!=null)
            fn();
    };
    obj.toPage=function (){
        //TODO
    };
    //设定当前工作对象
    obj.setIndex=function(index,fn){
        //设定工作对象
        obj.single = obj.list[index];
        if(fn!=null)
            fn();
    };
    //打开editor对话框
    obj.editor=function(){
        //渲染对话框,弹出editor对话框
        //TODO 构建对话框的样式
        var s = '<form role="form">';
        for(var i in obj.comments)
        {
            s += '<div class="form-group">';
            //TODO 根据数据格式进行输出模版定制
            s += '<lable for="exampleInputEmail1">'+obj.comments[i]+'</lable>';
            if(type[i]=='DATETIME') {
                s += "<input>";//TODO
            }else if(false){

            }else{
                s += "<input>";//TODO
            }
            s += '<div>'
        }
        s += "</form>";

        //TODO 模拟弹窗弹出editor对话框
    };
    //渲染表格
    obj.render=function () {
        var s = "<table class='table'><tr>";
        for(var i in obj.comments)
        {
            s += "<th>"+obj.comments[i]+"</th>";
        }
        s += "</tr>";
        for(var i in obj.list)
        {
            s += "<tr>";
            for(var j in obj.list[i])
            {
                s += "<td>"+obj.list[i][j]+"</td>";
            }
            s += "</tr>";
        }
        s += "</table>";
        $(obj.container).html(s);
    };
    obj.list=function(fn){
        req(obj.url.toList(),{offset:obj.offset,limit:obj.size},function (data) {
           obj.list = data.result;
            if(fn!=null)
                fn();
        });
    };
    obj.update=function(fn){
        req(obj.url.toUpdate(),obj.single,function (data) {
            if(data.status==0) {
                if (fn != null)
                    fn();
            }
        });
    };
    obj.del=function(fn){
        req(obj.url.toCreate(),obj.single.userid,function (data) {
            if(data.status==0) {
                if (fn != null)
                    fn();
            }
        });
    };
    obj.create=function(fn){
        req(obj.url.toCreate(),obj.single,function (data) {
            if(data.status==0) {
                if (fn != null)
                    fn();
            }
        });
    };
    return obj;
}