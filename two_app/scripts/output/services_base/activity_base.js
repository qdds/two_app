/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (activity) {
    //判断是否为空
    if (activity != null) {
      return models.activity.create(activity);
    } else {
      return Promise.reject("activity对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (activity,attributes) {
    if (typeof(activity) === 'string') {
      return models.activity.findOne({where: {activityid: activity},attributes:attributes});
    }
    else {
      if (activity != null) {
        if (activity.activityid != null) {
          return models.activity.findOne({where: {activityid: activity.activityid},attributes:attributes});
        }
      }
    }
    return Promise.reject("activity对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.activity.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("activity对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (activity) {
    if (activity != null) {
      if (activity.activityid != null) {
        return models.activity.update(activity, {where: {activityid: activity.activityid}});
      }
    }
    return Promise.reject("activity对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (activity,obj) {
    if (activity != null) {
      return models.activity.update(activity, {where: obj});
    }
    return Promise.reject("activity对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (activity) {
    if (typeof(activity) === 'string') {
      return models.activity.delete({where: {activityid: activity}});
    }
    else {
      if (activity != null) {
        if (activity.activityid != null) {
          return models.activity.delete({where: {activityid: activity.activityid}});
        }
      }
    }
    return Promise.reject("activity对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (activity) {

      if (activity != null) {
        return models.activity.delete({where: activity});
      }

    return Promise.reject("activity对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.activity.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.activity.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['activity'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.activity.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.activity.count({where: where});
  }

  
  //按照type索引列出从n到n+size,size小于50，默认为20
  ,listPage_type: function (offset,limit,type,st,et,attributes) {
    var where = {type:type};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['activity'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.activity.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_type 参数类型有误");
  }

  //按type索引和时间范围统计created数
  ,count_type: function(type,st,et)
  {
    var where = {type:type};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.activity.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['activity'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.activity.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.activity.count({where: where});
  }
  
  //按照bengin_datetime索引列出从n到n+size,size小于50，默认为20
  ,listPage_bengin_datetime: function (offset,limit,bengin_datetime,st,et,attributes) {
    var where = {bengin_datetime:bengin_datetime};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['activity'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.activity.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_bengin_datetime 参数类型有误");
  }

  //按bengin_datetime索引和时间范围统计created数
  ,count_bengin_datetime: function(bengin_datetime,st,et)
  {
    var where = {bengin_datetime:bengin_datetime};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.activity.count({where: where});
  }
  
  //按照finish_datetime索引列出从n到n+size,size小于50，默认为20
  ,listPage_finish_datetime: function (offset,limit,finish_datetime,st,et,attributes) {
    var where = {finish_datetime:finish_datetime};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['activity'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.activity.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_finish_datetime 参数类型有误");
  }

  //按finish_datetime索引和时间范围统计created数
  ,count_finish_datetime: function(finish_datetime,st,et)
  {
    var where = {finish_datetime:finish_datetime};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.activity.count({where: where});
  }
  
  //按照power索引列出从n到n+size,size小于50，默认为20
  ,listPage_power: function (offset,limit,power,st,et,attributes) {
    var where = {power:power};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['activity'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.activity.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_power 参数类型有误");
  }

  //按power索引和时间范围统计created数
  ,count_power: function(power,st,et)
  {
    var where = {power:power};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.activity.count({where: where});
  }
  
  //按照title索引列出从n到n+size,size小于50，默认为20
  ,listPage_title: function (offset,limit,title,st,et,attributes) {
    var where = {title:title};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['activity'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.activity.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_title 参数类型有误");
  }

  //按title索引和时间范围统计created数
  ,count_title: function(title,st,et)
  {
    var where = {title:title};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.activity.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['activity'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.activity.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.activity.count({where: where});
  }
  
};
