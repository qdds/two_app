/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (operator) {
    //判断是否为空
    if (operator != null) {
      return models.operator.create(operator);
    } else {
      return Promise.reject("operator对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (operator,attributes) {
    if (typeof(operator) === 'string') {
      return models.operator.findOne({where: {account: operator},attributes:attributes});
    }
    else {
      if (operator != null) {
        if (operator.account != null) {
          return models.operator.findOne({where: {account: operator.account},attributes:attributes});
        }
      }
    }
    return Promise.reject("operator对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.operator.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("operator对象不能为空");
  }

  
  ,findOne_mobile: function (mobile,attributes) {
    if (mobile) {
      return models.operator.findOne({where: {mobile:mobile},attributes:attributes});
    }
    else {
      return Promise.reject("mobile不能为空");
    }
  }
  

  //根据主键更新记录
  ,update: function (operator) {
    if (operator != null) {
      if (operator.account != null) {
        return models.operator.update(operator, {where: {account: operator.account}});
      }
    }
    return Promise.reject("operator对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (operator,obj) {
    if (operator != null) {
      return models.operator.update(operator, {where: obj});
    }
    return Promise.reject("operator对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (operator) {
    if (typeof(operator) === 'string') {
      return models.operator.delete({where: {account: operator}});
    }
    else {
      if (operator != null) {
        if (operator.account != null) {
          return models.operator.delete({where: {account: operator.account}});
        }
      }
    }
    return Promise.reject("operator对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (operator) {

      if (operator != null) {
        return models.operator.delete({where: operator});
      }

    return Promise.reject("operator对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.operator.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.operator.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['operator'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.operator.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.operator.count({where: where});
  }

  
  //按照power索引列出从n到n+size,size小于50，默认为20
  ,listPage_power: function (offset,limit,power,st,et,attributes) {
    var where = {power:power};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['operator'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.operator.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_power 参数类型有误");
  }

  //按power索引和时间范围统计created数
  ,count_power: function(power,st,et)
  {
    var where = {power:power};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.operator.count({where: where});
  }
  
};
