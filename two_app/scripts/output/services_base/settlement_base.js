/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (settlement) {
    //判断是否为空
    if (settlement != null) {
      return models.settlement.create(settlement);
    } else {
      return Promise.reject("settlement对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (settlement,attributes) {
    if (typeof(settlement) === 'string') {
      return models.settlement.findOne({where: {id: settlement},attributes:attributes});
    }
    else {
      if (settlement != null) {
        if (settlement.id != null) {
          return models.settlement.findOne({where: {id: settlement.id},attributes:attributes});
        }
      }
    }
    return Promise.reject("settlement对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.settlement.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("settlement对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (settlement) {
    if (settlement != null) {
      if (settlement.id != null) {
        return models.settlement.update(settlement, {where: {id: settlement.id}});
      }
    }
    return Promise.reject("settlement对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (settlement,obj) {
    if (settlement != null) {
      return models.settlement.update(settlement, {where: obj});
    }
    return Promise.reject("settlement对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (settlement) {
    if (typeof(settlement) === 'string') {
      return models.settlement.delete({where: {id: settlement}});
    }
    else {
      if (settlement != null) {
        if (settlement.id != null) {
          return models.settlement.delete({where: {id: settlement.id}});
        }
      }
    }
    return Promise.reject("settlement对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (settlement) {

      if (settlement != null) {
        return models.settlement.delete({where: settlement});
      }

    return Promise.reject("settlement对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.settlement.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.settlement.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['settlement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.settlement.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.settlement.count({where: where});
  }

  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['settlement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.settlement.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.settlement.count({where: where});
  }
  
  //按照settlement_amount索引列出从n到n+size,size小于50，默认为20
  ,listPage_settlement_amount: function (offset,limit,settlement_amount,st,et,attributes) {
    var where = {settlement_amount:settlement_amount};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['settlement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.settlement.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_settlement_amount 参数类型有误");
  }

  //按settlement_amount索引和时间范围统计created数
  ,count_settlement_amount: function(settlement_amount,st,et)
  {
    var where = {settlement_amount:settlement_amount};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.settlement.count({where: where});
  }
  
  //按照settlement_datetime索引列出从n到n+size,size小于50，默认为20
  ,listPage_settlement_datetime: function (offset,limit,settlement_datetime,st,et,attributes) {
    var where = {settlement_datetime:settlement_datetime};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['settlement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.settlement.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_settlement_datetime 参数类型有误");
  }

  //按settlement_datetime索引和时间范围统计created数
  ,count_settlement_datetime: function(settlement_datetime,st,et)
  {
    var where = {settlement_datetime:settlement_datetime};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.settlement.count({where: where});
  }
  
  //按照status索引列出从n到n+size,size小于50，默认为20
  ,listPage_status: function (offset,limit,status,st,et,attributes) {
    var where = {status:status};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['settlement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.settlement.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_status 参数类型有误");
  }

  //按status索引和时间范围统计created数
  ,count_status: function(status,st,et)
  {
    var where = {status:status};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.settlement.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['settlement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.settlement.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.settlement.count({where: where});
  }
  
};
