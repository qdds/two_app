/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (achievement) {
    //判断是否为空
    if (achievement != null) {
      return models.achievement.create(achievement);
    } else {
      return Promise.reject("achievement对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (achievement,attributes) {
    if (typeof(achievement) === 'string') {
      return models.achievement.findOne({where: {salesmanid: achievement},attributes:attributes});
    }
    else {
      if (achievement != null) {
        if (achievement.salesmanid != null) {
          return models.achievement.findOne({where: {salesmanid: achievement.salesmanid},attributes:attributes});
        }
      }
    }
    return Promise.reject("achievement对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.achievement.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("achievement对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (achievement) {
    if (achievement != null) {
      if (achievement.salesmanid != null) {
        return models.achievement.update(achievement, {where: {salesmanid: achievement.salesmanid}});
      }
    }
    return Promise.reject("achievement对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (achievement,obj) {
    if (achievement != null) {
      return models.achievement.update(achievement, {where: obj});
    }
    return Promise.reject("achievement对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (achievement) {
    if (typeof(achievement) === 'string') {
      return models.achievement.delete({where: {salesmanid: achievement}});
    }
    else {
      if (achievement != null) {
        if (achievement.salesmanid != null) {
          return models.achievement.delete({where: {salesmanid: achievement.salesmanid}});
        }
      }
    }
    return Promise.reject("achievement对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (achievement) {

      if (achievement != null) {
        return models.achievement.delete({where: achievement});
      }

    return Promise.reject("achievement对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.achievement.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.achievement.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['achievement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.achievement.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.achievement.count({where: where});
  }

  
  //按照commission索引列出从n到n+size,size小于50，默认为20
  ,listPage_commission: function (offset,limit,commission,st,et,attributes) {
    var where = {commission:commission};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['achievement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.achievement.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_commission 参数类型有误");
  }

  //按commission索引和时间范围统计created数
  ,count_commission: function(commission,st,et)
  {
    var where = {commission:commission};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.achievement.count({where: where});
  }
  
  //按照sales索引列出从n到n+size,size小于50，默认为20
  ,listPage_sales: function (offset,limit,sales,st,et,attributes) {
    var where = {sales:sales};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['achievement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.achievement.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_sales 参数类型有误");
  }

  //按sales索引和时间范围统计created数
  ,count_sales: function(sales,st,et)
  {
    var where = {sales:sales};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.achievement.count({where: where});
  }
  
  //按照month索引列出从n到n+size,size小于50，默认为20
  ,listPage_month: function (offset,limit,month,st,et,attributes) {
    var where = {month:month};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['achievement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.achievement.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_month 参数类型有误");
  }

  //按month索引和时间范围统计created数
  ,count_month: function(month,st,et)
  {
    var where = {month:month};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.achievement.count({where: where});
  }
  
  //按照salesmanid索引列出从n到n+size,size小于50，默认为20
  ,listPage_salesmanid: function (offset,limit,salesmanid,st,et,attributes) {
    var where = {salesmanid:salesmanid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['achievement'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.achievement.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_salesmanid 参数类型有误");
  }

  //按salesmanid索引和时间范围统计created数
  ,count_salesmanid: function(salesmanid,st,et)
  {
    var where = {salesmanid:salesmanid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.achievement.count({where: where});
  }
  
};
