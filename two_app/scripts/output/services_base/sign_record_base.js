/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (sign_record) {
    //判断是否为空
    if (sign_record != null) {
      return models.sign_record.create(sign_record);
    } else {
      return Promise.reject("sign_record对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (sign_record,attributes) {
    if (typeof(sign_record) === 'string') {
      return models.sign_record.findOne({where: {id: sign_record},attributes:attributes});
    }
    else {
      if (sign_record != null) {
        if (sign_record.id != null) {
          return models.sign_record.findOne({where: {id: sign_record.id},attributes:attributes});
        }
      }
    }
    return Promise.reject("sign_record对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.sign_record.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("sign_record对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (sign_record) {
    if (sign_record != null) {
      if (sign_record.id != null) {
        return models.sign_record.update(sign_record, {where: {id: sign_record.id}});
      }
    }
    return Promise.reject("sign_record对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (sign_record,obj) {
    if (sign_record != null) {
      return models.sign_record.update(sign_record, {where: obj});
    }
    return Promise.reject("sign_record对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (sign_record) {
    if (typeof(sign_record) === 'string') {
      return models.sign_record.delete({where: {id: sign_record}});
    }
    else {
      if (sign_record != null) {
        if (sign_record.id != null) {
          return models.sign_record.delete({where: {id: sign_record.id}});
        }
      }
    }
    return Promise.reject("sign_record对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (sign_record) {

      if (sign_record != null) {
        return models.sign_record.delete({where: sign_record});
      }

    return Promise.reject("sign_record对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.sign_record.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.sign_record.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['sign_record'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.sign_record.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.sign_record.count({where: where});
  }

  
  //按照userid索引列出从n到n+size,size小于50，默认为20
  ,listPage_userid: function (offset,limit,userid,st,et,attributes) {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['sign_record'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.sign_record.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_userid 参数类型有误");
  }

  //按userid索引和时间范围统计created数
  ,count_userid: function(userid,st,et)
  {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.sign_record.count({where: where});
  }
  
  //按照sign_datetime索引列出从n到n+size,size小于50，默认为20
  ,listPage_sign_datetime: function (offset,limit,sign_datetime,st,et,attributes) {
    var where = {sign_datetime:sign_datetime};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['sign_record'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.sign_record.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_sign_datetime 参数类型有误");
  }

  //按sign_datetime索引和时间范围统计created数
  ,count_sign_datetime: function(sign_datetime,st,et)
  {
    var where = {sign_datetime:sign_datetime};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.sign_record.count({where: where});
  }
  
  //按照constantly_sign索引列出从n到n+size,size小于50，默认为20
  ,listPage_constantly_sign: function (offset,limit,constantly_sign,st,et,attributes) {
    var where = {constantly_sign:constantly_sign};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['sign_record'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.sign_record.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_constantly_sign 参数类型有误");
  }

  //按constantly_sign索引和时间范围统计created数
  ,count_constantly_sign: function(constantly_sign,st,et)
  {
    var where = {constantly_sign:constantly_sign};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.sign_record.count({where: where});
  }
  
  //按照userid索引列出从n到n+size,size小于50，默认为20
  ,listPage_userid: function (offset,limit,userid,st,et,attributes) {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['sign_record'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.sign_record.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_userid 参数类型有误");
  }

  //按userid索引和时间范围统计created数
  ,count_userid: function(userid,st,et)
  {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.sign_record.count({where: where});
  }
  
};
