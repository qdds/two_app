/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (coupon) {
    //判断是否为空
    if (coupon != null) {
      return models.coupon.create(coupon);
    } else {
      return Promise.reject("coupon对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (coupon,attributes) {
    if (typeof(coupon) === 'string') {
      return models.coupon.findOne({where: {couponid: coupon},attributes:attributes});
    }
    else {
      if (coupon != null) {
        if (coupon.couponid != null) {
          return models.coupon.findOne({where: {couponid: coupon.couponid},attributes:attributes});
        }
      }
    }
    return Promise.reject("coupon对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.coupon.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("coupon对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (coupon) {
    if (coupon != null) {
      if (coupon.couponid != null) {
        return models.coupon.update(coupon, {where: {couponid: coupon.couponid}});
      }
    }
    return Promise.reject("coupon对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (coupon,obj) {
    if (coupon != null) {
      return models.coupon.update(coupon, {where: obj});
    }
    return Promise.reject("coupon对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (coupon) {
    if (typeof(coupon) === 'string') {
      return models.coupon.delete({where: {couponid: coupon}});
    }
    else {
      if (coupon != null) {
        if (coupon.couponid != null) {
          return models.coupon.delete({where: {couponid: coupon.couponid}});
        }
      }
    }
    return Promise.reject("coupon对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (coupon) {

      if (coupon != null) {
        return models.coupon.delete({where: coupon});
      }

    return Promise.reject("coupon对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.coupon.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.coupon.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }

  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }
  
  //按照type索引列出从n到n+size,size小于50，默认为20
  ,listPage_type: function (offset,limit,type,st,et,attributes) {
    var where = {type:type};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_type 参数类型有误");
  }

  //按type索引和时间范围统计created数
  ,count_type: function(type,st,et)
  {
    var where = {type:type};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }
  
  //按照trigger_amount索引列出从n到n+size,size小于50，默认为20
  ,listPage_trigger_amount: function (offset,limit,trigger_amount,st,et,attributes) {
    var where = {trigger_amount:trigger_amount};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_trigger_amount 参数类型有误");
  }

  //按trigger_amount索引和时间范围统计created数
  ,count_trigger_amount: function(trigger_amount,st,et)
  {
    var where = {trigger_amount:trigger_amount};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }
  
  //按照discount_amount索引列出从n到n+size,size小于50，默认为20
  ,listPage_discount_amount: function (offset,limit,discount_amount,st,et,attributes) {
    var where = {discount_amount:discount_amount};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_discount_amount 参数类型有误");
  }

  //按discount_amount索引和时间范围统计created数
  ,count_discount_amount: function(discount_amount,st,et)
  {
    var where = {discount_amount:discount_amount};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }
  
  //按照quantity索引列出从n到n+size,size小于50，默认为20
  ,listPage_quantity: function (offset,limit,quantity,st,et,attributes) {
    var where = {quantity:quantity};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_quantity 参数类型有误");
  }

  //按quantity索引和时间范围统计created数
  ,count_quantity: function(quantity,st,et)
  {
    var where = {quantity:quantity};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }
  
  //按照remaining_quantity索引列出从n到n+size,size小于50，默认为20
  ,listPage_remaining_quantity: function (offset,limit,remaining_quantity,st,et,attributes) {
    var where = {remaining_quantity:remaining_quantity};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_remaining_quantity 参数类型有误");
  }

  //按remaining_quantity索引和时间范围统计created数
  ,count_remaining_quantity: function(remaining_quantity,st,et)
  {
    var where = {remaining_quantity:remaining_quantity};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }
  
  //按照begin_datetime索引列出从n到n+size,size小于50，默认为20
  ,listPage_begin_datetime: function (offset,limit,begin_datetime,st,et,attributes) {
    var where = {begin_datetime:begin_datetime};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_begin_datetime 参数类型有误");
  }

  //按begin_datetime索引和时间范围统计created数
  ,count_begin_datetime: function(begin_datetime,st,et)
  {
    var where = {begin_datetime:begin_datetime};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }
  
  //按照finish_datetime索引列出从n到n+size,size小于50，默认为20
  ,listPage_finish_datetime: function (offset,limit,finish_datetime,st,et,attributes) {
    var where = {finish_datetime:finish_datetime};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_finish_datetime 参数类型有误");
  }

  //按finish_datetime索引和时间范围统计created数
  ,count_finish_datetime: function(finish_datetime,st,et)
  {
    var where = {finish_datetime:finish_datetime};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }
  
  //按照status索引列出从n到n+size,size小于50，默认为20
  ,listPage_status: function (offset,limit,status,st,et,attributes) {
    var where = {status:status};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_status 参数类型有误");
  }

  //按status索引和时间范围统计created数
  ,count_status: function(status,st,et)
  {
    var where = {status:status};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.coupon.count({where: where});
  }
  
};
