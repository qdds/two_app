/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (beautician) {
    //判断是否为空
    if (beautician != null) {
      return models.beautician.create(beautician);
    } else {
      return Promise.reject("beautician对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (beautician,attributes) {
    if (typeof(beautician) === 'string') {
      return models.beautician.findOne({where: {beauticianid: beautician},attributes:attributes});
    }
    else {
      if (beautician != null) {
        if (beautician.beauticianid != null) {
          return models.beautician.findOne({where: {beauticianid: beautician.beauticianid},attributes:attributes});
        }
      }
    }
    return Promise.reject("beautician对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.beautician.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("beautician对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (beautician) {
    if (beautician != null) {
      if (beautician.beauticianid != null) {
        return models.beautician.update(beautician, {where: {beauticianid: beautician.beauticianid}});
      }
    }
    return Promise.reject("beautician对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (beautician,obj) {
    if (beautician != null) {
      return models.beautician.update(beautician, {where: obj});
    }
    return Promise.reject("beautician对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (beautician) {
    if (typeof(beautician) === 'string') {
      return models.beautician.delete({where: {beauticianid: beautician}});
    }
    else {
      if (beautician != null) {
        if (beautician.beauticianid != null) {
          return models.beautician.delete({where: {beauticianid: beautician.beauticianid}});
        }
      }
    }
    return Promise.reject("beautician对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (beautician) {

      if (beautician != null) {
        return models.beautician.delete({where: beautician});
      }

    return Promise.reject("beautician对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.beautician.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.beautician.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['beautician'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.beautician.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.beautician.count({where: where});
  }

  
  //按照real_name索引列出从n到n+size,size小于50，默认为20
  ,listPage_real_name: function (offset,limit,real_name,st,et,attributes) {
    var where = {real_name:real_name};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['beautician'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.beautician.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_real_name 参数类型有误");
  }

  //按real_name索引和时间范围统计created数
  ,count_real_name: function(real_name,st,et)
  {
    var where = {real_name:real_name};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.beautician.count({where: where});
  }
  
  //按照mobile索引列出从n到n+size,size小于50，默认为20
  ,listPage_mobile: function (offset,limit,mobile,st,et,attributes) {
    var where = {mobile:mobile};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['beautician'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.beautician.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_mobile 参数类型有误");
  }

  //按mobile索引和时间范围统计created数
  ,count_mobile: function(mobile,st,et)
  {
    var where = {mobile:mobile};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.beautician.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['beautician'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.beautician.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.beautician.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['beautician'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.beautician.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.beautician.count({where: where});
  }
  
};
