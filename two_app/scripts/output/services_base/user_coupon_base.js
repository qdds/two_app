/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (user_coupon) {
    //判断是否为空
    if (user_coupon != null) {
      return models.user_coupon.create(user_coupon);
    } else {
      return Promise.reject("user_coupon对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (user_coupon,attributes) {
    if (typeof(user_coupon) === 'string') {
      return models.user_coupon.findOne({where: {id: user_coupon},attributes:attributes});
    }
    else {
      if (user_coupon != null) {
        if (user_coupon.id != null) {
          return models.user_coupon.findOne({where: {id: user_coupon.id},attributes:attributes});
        }
      }
    }
    return Promise.reject("user_coupon对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.user_coupon.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("user_coupon对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (user_coupon) {
    if (user_coupon != null) {
      if (user_coupon.id != null) {
        return models.user_coupon.update(user_coupon, {where: {id: user_coupon.id}});
      }
    }
    return Promise.reject("user_coupon对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (user_coupon,obj) {
    if (user_coupon != null) {
      return models.user_coupon.update(user_coupon, {where: obj});
    }
    return Promise.reject("user_coupon对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (user_coupon) {
    if (typeof(user_coupon) === 'string') {
      return models.user_coupon.delete({where: {id: user_coupon}});
    }
    else {
      if (user_coupon != null) {
        if (user_coupon.id != null) {
          return models.user_coupon.delete({where: {id: user_coupon.id}});
        }
      }
    }
    return Promise.reject("user_coupon对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (user_coupon) {

      if (user_coupon != null) {
        return models.user_coupon.delete({where: user_coupon});
      }

    return Promise.reject("user_coupon对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.user_coupon.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.user_coupon.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user_coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user_coupon.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user_coupon.count({where: where});
  }

  
  //按照couponid索引列出从n到n+size,size小于50，默认为20
  ,listPage_couponid: function (offset,limit,couponid,st,et,attributes) {
    var where = {couponid:couponid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user_coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user_coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_couponid 参数类型有误");
  }

  //按couponid索引和时间范围统计created数
  ,count_couponid: function(couponid,st,et)
  {
    var where = {couponid:couponid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user_coupon.count({where: where});
  }
  
  //按照userid索引列出从n到n+size,size小于50，默认为20
  ,listPage_userid: function (offset,limit,userid,st,et,attributes) {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user_coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user_coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_userid 参数类型有误");
  }

  //按userid索引和时间范围统计created数
  ,count_userid: function(userid,st,et)
  {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user_coupon.count({where: where});
  }
  
  //按照deadline索引列出从n到n+size,size小于50，默认为20
  ,listPage_deadline: function (offset,limit,deadline,st,et,attributes) {
    var where = {deadline:deadline};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user_coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user_coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_deadline 参数类型有误");
  }

  //按deadline索引和时间范围统计created数
  ,count_deadline: function(deadline,st,et)
  {
    var where = {deadline:deadline};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user_coupon.count({where: where});
  }
  
  //按照status索引列出从n到n+size,size小于50，默认为20
  ,listPage_status: function (offset,limit,status,st,et,attributes) {
    var where = {status:status};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user_coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user_coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_status 参数类型有误");
  }

  //按status索引和时间范围统计created数
  ,count_status: function(status,st,et)
  {
    var where = {status:status};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user_coupon.count({where: where});
  }
  
  //按照original索引列出从n到n+size,size小于50，默认为20
  ,listPage_original: function (offset,limit,original,st,et,attributes) {
    var where = {original:original};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user_coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user_coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_original 参数类型有误");
  }

  //按original索引和时间范围统计created数
  ,count_original: function(original,st,et)
  {
    var where = {original:original};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user_coupon.count({where: where});
  }
  
  //按照couponid索引列出从n到n+size,size小于50，默认为20
  ,listPage_couponid: function (offset,limit,couponid,st,et,attributes) {
    var where = {couponid:couponid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user_coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user_coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_couponid 参数类型有误");
  }

  //按couponid索引和时间范围统计created数
  ,count_couponid: function(couponid,st,et)
  {
    var where = {couponid:couponid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user_coupon.count({where: where});
  }
  
  //按照userid索引列出从n到n+size,size小于50，默认为20
  ,listPage_userid: function (offset,limit,userid,st,et,attributes) {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user_coupon'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user_coupon.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_userid 参数类型有误");
  }

  //按userid索引和时间范围统计created数
  ,count_userid: function(userid,st,et)
  {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user_coupon.count({where: where});
  }
  
};
