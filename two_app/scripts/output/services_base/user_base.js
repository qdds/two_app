/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (user) {
    //判断是否为空
    if (user != null) {
      return models.user.create(user);
    } else {
      return Promise.reject("user对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (user,attributes) {
    if (typeof(user) === 'string') {
      return models.user.findOne({where: {userid: user},attributes:attributes});
    }
    else {
      if (user != null) {
        if (user.userid != null) {
          return models.user.findOne({where: {userid: user.userid},attributes:attributes});
        }
      }
    }
    return Promise.reject("user对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.user.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("user对象不能为空");
  }

  
  ,findOne_openid: function (openid,attributes) {
    if (openid) {
      return models.user.findOne({where: {openid:openid},attributes:attributes});
    }
    else {
      return Promise.reject("openid不能为空");
    }
  }
  
  ,findOne_mobile: function (mobile,attributes) {
    if (mobile) {
      return models.user.findOne({where: {mobile:mobile},attributes:attributes});
    }
    else {
      return Promise.reject("mobile不能为空");
    }
  }
  

  //根据主键更新记录
  ,update: function (user) {
    if (user != null) {
      if (user.userid != null) {
        return models.user.update(user, {where: {userid: user.userid}});
      }
    }
    return Promise.reject("user对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (user,obj) {
    if (user != null) {
      return models.user.update(user, {where: obj});
    }
    return Promise.reject("user对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (user) {
    if (typeof(user) === 'string') {
      return models.user.delete({where: {userid: user}});
    }
    else {
      if (user != null) {
        if (user.userid != null) {
          return models.user.delete({where: {userid: user.userid}});
        }
      }
    }
    return Promise.reject("user对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (user) {

      if (user != null) {
        return models.user.delete({where: user});
      }

    return Promise.reject("user对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.user.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.user.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user.count({where: where});
  }

  
  //按照register_way索引列出从n到n+size,size小于50，默认为20
  ,listPage_register_way: function (offset,limit,register_way,st,et,attributes) {
    var where = {register_way:register_way};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_register_way 参数类型有误");
  }

  //按register_way索引和时间范围统计created数
  ,count_register_way: function(register_way,st,et)
  {
    var where = {register_way:register_way};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user.count({where: where});
  }
  
  //按照userid索引列出从n到n+size,size小于50，默认为20
  ,listPage_userid: function (offset,limit,userid,st,et,attributes) {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_userid 参数类型有误");
  }

  //按userid索引和时间范围统计created数
  ,count_userid: function(userid,st,et)
  {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user.count({where: where});
  }
  
  //按照age索引列出从n到n+size,size小于50，默认为20
  ,listPage_age: function (offset,limit,age,st,et,attributes) {
    var where = {age:age};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_age 参数类型有误");
  }

  //按age索引和时间范围统计created数
  ,count_age: function(age,st,et)
  {
    var where = {age:age};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user.count({where: where});
  }
  
  //按照sex索引列出从n到n+size,size小于50，默认为20
  ,listPage_sex: function (offset,limit,sex,st,et,attributes) {
    var where = {sex:sex};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_sex 参数类型有误");
  }

  //按sex索引和时间范围统计created数
  ,count_sex: function(sex,st,et)
  {
    var where = {sex:sex};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['user'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.user.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.user.count({where: where});
  }
  
};
