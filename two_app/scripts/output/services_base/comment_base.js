/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (comment) {
    //判断是否为空
    if (comment != null) {
      return models.comment.create(comment);
    } else {
      return Promise.reject("comment对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (comment,attributes) {
    if (typeof(comment) === 'string') {
      return models.comment.findOne({where: {commentid: comment},attributes:attributes});
    }
    else {
      if (comment != null) {
        if (comment.commentid != null) {
          return models.comment.findOne({where: {commentid: comment.commentid},attributes:attributes});
        }
      }
    }
    return Promise.reject("comment对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.comment.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("comment对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (comment) {
    if (comment != null) {
      if (comment.commentid != null) {
        return models.comment.update(comment, {where: {commentid: comment.commentid}});
      }
    }
    return Promise.reject("comment对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (comment,obj) {
    if (comment != null) {
      return models.comment.update(comment, {where: obj});
    }
    return Promise.reject("comment对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (comment) {
    if (typeof(comment) === 'string') {
      return models.comment.delete({where: {commentid: comment}});
    }
    else {
      if (comment != null) {
        if (comment.commentid != null) {
          return models.comment.delete({where: {commentid: comment.commentid}});
        }
      }
    }
    return Promise.reject("comment对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (comment) {

      if (comment != null) {
        return models.comment.delete({where: comment});
      }

    return Promise.reject("comment对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.comment.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.comment.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }

  
  //按照userid索引列出从n到n+size,size小于50，默认为20
  ,listPage_userid: function (offset,limit,userid,st,et,attributes) {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_userid 参数类型有误");
  }

  //按userid索引和时间范围统计created数
  ,count_userid: function(userid,st,et)
  {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }
  
  //按照productid索引列出从n到n+size,size小于50，默认为20
  ,listPage_productid: function (offset,limit,productid,st,et,attributes) {
    var where = {productid:productid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_productid 参数类型有误");
  }

  //按productid索引和时间范围统计created数
  ,count_productid: function(productid,st,et)
  {
    var where = {productid:productid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }
  
  //按照shelvesid索引列出从n到n+size,size小于50，默认为20
  ,listPage_shelvesid: function (offset,limit,shelvesid,st,et,attributes) {
    var where = {shelvesid:shelvesid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_shelvesid 参数类型有误");
  }

  //按shelvesid索引和时间范围统计created数
  ,count_shelvesid: function(shelvesid,st,et)
  {
    var where = {shelvesid:shelvesid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }
  
  //按照commentid索引列出从n到n+size,size小于50，默认为20
  ,listPage_commentid: function (offset,limit,commentid,st,et,attributes) {
    var where = {commentid:commentid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_commentid 参数类型有误");
  }

  //按commentid索引和时间范围统计created数
  ,count_commentid: function(commentid,st,et)
  {
    var where = {commentid:commentid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }
  
  //按照userid索引列出从n到n+size,size小于50，默认为20
  ,listPage_userid: function (offset,limit,userid,st,et,attributes) {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_userid 参数类型有误");
  }

  //按userid索引和时间范围统计created数
  ,count_userid: function(userid,st,et)
  {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }
  
  //按照shelvesid索引列出从n到n+size,size小于50，默认为20
  ,listPage_shelvesid: function (offset,limit,shelvesid,st,et,attributes) {
    var where = {shelvesid:shelvesid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_shelvesid 参数类型有误");
  }

  //按shelvesid索引和时间范围统计created数
  ,count_shelvesid: function(shelvesid,st,et)
  {
    var where = {shelvesid:shelvesid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }
  
  //按照productid索引列出从n到n+size,size小于50，默认为20
  ,listPage_productid: function (offset,limit,productid,st,et,attributes) {
    var where = {productid:productid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_productid 参数类型有误");
  }

  //按productid索引和时间范围统计created数
  ,count_productid: function(productid,st,et)
  {
    var where = {productid:productid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }
  
  //按照orderid索引列出从n到n+size,size小于50，默认为20
  ,listPage_orderid: function (offset,limit,orderid,st,et,attributes) {
    var where = {orderid:orderid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_orderid 参数类型有误");
  }

  //按orderid索引和时间范围统计created数
  ,count_orderid: function(orderid,st,et)
  {
    var where = {orderid:orderid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['comment'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.comment.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.comment.count({where: where});
  }
  
};
