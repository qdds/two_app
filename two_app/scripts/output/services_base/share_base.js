/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (share) {
    //判断是否为空
    if (share != null) {
      return models.share.create(share);
    } else {
      return Promise.reject("share对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (share,attributes) {
    if (typeof(share) === 'string') {
      return models.share.findOne({where: {shareid: share},attributes:attributes});
    }
    else {
      if (share != null) {
        if (share.shareid != null) {
          return models.share.findOne({where: {shareid: share.shareid},attributes:attributes});
        }
      }
    }
    return Promise.reject("share对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.share.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("share对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (share) {
    if (share != null) {
      if (share.shareid != null) {
        return models.share.update(share, {where: {shareid: share.shareid}});
      }
    }
    return Promise.reject("share对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (share,obj) {
    if (share != null) {
      return models.share.update(share, {where: obj});
    }
    return Promise.reject("share对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (share) {
    if (typeof(share) === 'string') {
      return models.share.delete({where: {shareid: share}});
    }
    else {
      if (share != null) {
        if (share.shareid != null) {
          return models.share.delete({where: {shareid: share.shareid}});
        }
      }
    }
    return Promise.reject("share对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (share) {

      if (share != null) {
        return models.share.delete({where: share});
      }

    return Promise.reject("share对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.share.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.share.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['share'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.share.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.share.count({where: where});
  }

  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['share'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.share.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.share.count({where: where});
  }
  
  //按照userid索引列出从n到n+size,size小于50，默认为20
  ,listPage_userid: function (offset,limit,userid,st,et,attributes) {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['share'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.share.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_userid 参数类型有误");
  }

  //按userid索引和时间范围统计created数
  ,count_userid: function(userid,st,et)
  {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.share.count({where: where});
  }
  
  //按照activityid索引列出从n到n+size,size小于50，默认为20
  ,listPage_activityid: function (offset,limit,activityid,st,et,attributes) {
    var where = {activityid:activityid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['share'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.share.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_activityid 参数类型有误");
  }

  //按activityid索引和时间范围统计created数
  ,count_activityid: function(activityid,st,et)
  {
    var where = {activityid:activityid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.share.count({where: where});
  }
  
};
