/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (ecommerce) {
    //判断是否为空
    if (ecommerce != null) {
      return models.ecommerce.create(ecommerce);
    } else {
      return Promise.reject("ecommerce对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (ecommerce,attributes) {
    if (typeof(ecommerce) === 'string') {
      return models.ecommerce.findOne({where: {shelvesid: ecommerce},attributes:attributes});
    }
    else {
      if (ecommerce != null) {
        if (ecommerce.shelvesid != null) {
          return models.ecommerce.findOne({where: {shelvesid: ecommerce.shelvesid},attributes:attributes});
        }
      }
    }
    return Promise.reject("ecommerce对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.ecommerce.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("ecommerce对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (ecommerce) {
    if (ecommerce != null) {
      if (ecommerce.shelvesid != null) {
        return models.ecommerce.update(ecommerce, {where: {shelvesid: ecommerce.shelvesid}});
      }
    }
    return Promise.reject("ecommerce对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (ecommerce,obj) {
    if (ecommerce != null) {
      return models.ecommerce.update(ecommerce, {where: obj});
    }
    return Promise.reject("ecommerce对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (ecommerce) {
    if (typeof(ecommerce) === 'string') {
      return models.ecommerce.delete({where: {shelvesid: ecommerce}});
    }
    else {
      if (ecommerce != null) {
        if (ecommerce.shelvesid != null) {
          return models.ecommerce.delete({where: {shelvesid: ecommerce.shelvesid}});
        }
      }
    }
    return Promise.reject("ecommerce对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (ecommerce) {

      if (ecommerce != null) {
        return models.ecommerce.delete({where: ecommerce});
      }

    return Promise.reject("ecommerce对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.ecommerce.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.ecommerce.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }

  
  //按照shelves_type索引列出从n到n+size,size小于50，默认为20
  ,listPage_shelves_type: function (offset,limit,shelves_type,st,et,attributes) {
    var where = {shelves_type:shelves_type};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_shelves_type 参数类型有误");
  }

  //按shelves_type索引和时间范围统计created数
  ,count_shelves_type: function(shelves_type,st,et)
  {
    var where = {shelves_type:shelves_type};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照total_price索引列出从n到n+size,size小于50，默认为20
  ,listPage_total_price: function (offset,limit,total_price,st,et,attributes) {
    var where = {total_price:total_price};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_total_price 参数类型有误");
  }

  //按total_price索引和时间范围统计created数
  ,count_total_price: function(total_price,st,et)
  {
    var where = {total_price:total_price};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照selling_price索引列出从n到n+size,size小于50，默认为20
  ,listPage_selling_price: function (offset,limit,selling_price,st,et,attributes) {
    var where = {selling_price:selling_price};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_selling_price 参数类型有误");
  }

  //按selling_price索引和时间范围统计created数
  ,count_selling_price: function(selling_price,st,et)
  {
    var where = {selling_price:selling_price};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照amount索引列出从n到n+size,size小于50，默认为20
  ,listPage_amount: function (offset,limit,amount,st,et,attributes) {
    var where = {amount:amount};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_amount 参数类型有误");
  }

  //按amount索引和时间范围统计created数
  ,count_amount: function(amount,st,et)
  {
    var where = {amount:amount};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照purchased索引列出从n到n+size,size小于50，默认为20
  ,listPage_purchased: function (offset,limit,purchased,st,et,attributes) {
    var where = {purchased:purchased};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_purchased 参数类型有误");
  }

  //按purchased索引和时间范围统计created数
  ,count_purchased: function(purchased,st,et)
  {
    var where = {purchased:purchased};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照begin_datetime索引列出从n到n+size,size小于50，默认为20
  ,listPage_begin_datetime: function (offset,limit,begin_datetime,st,et,attributes) {
    var where = {begin_datetime:begin_datetime};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_begin_datetime 参数类型有误");
  }

  //按begin_datetime索引和时间范围统计created数
  ,count_begin_datetime: function(begin_datetime,st,et)
  {
    var where = {begin_datetime:begin_datetime};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照finish_datetime索引列出从n到n+size,size小于50，默认为20
  ,listPage_finish_datetime: function (offset,limit,finish_datetime,st,et,attributes) {
    var where = {finish_datetime:finish_datetime};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_finish_datetime 参数类型有误");
  }

  //按finish_datetime索引和时间范围统计created数
  ,count_finish_datetime: function(finish_datetime,st,et)
  {
    var where = {finish_datetime:finish_datetime};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照power索引列出从n到n+size,size小于50，默认为20
  ,listPage_power: function (offset,limit,power,st,et,attributes) {
    var where = {power:power};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_power 参数类型有误");
  }

  //按power索引和时间范围统计created数
  ,count_power: function(power,st,et)
  {
    var where = {power:power};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照carousel索引列出从n到n+size,size小于50，默认为20
  ,listPage_carousel: function (offset,limit,carousel,st,et,attributes) {
    var where = {carousel:carousel};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_carousel 参数类型有误");
  }

  //按carousel索引和时间范围统计created数
  ,count_carousel: function(carousel,st,et)
  {
    var where = {carousel:carousel};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照limit_purchased索引列出从n到n+size,size小于50，默认为20
  ,listPage_limit_purchased: function (offset,limit,limit_purchased,st,et,attributes) {
    var where = {limit_purchased:limit_purchased};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_limit_purchased 参数类型有误");
  }

  //按limit_purchased索引和时间范围统计created数
  ,count_limit_purchased: function(limit_purchased,st,et)
  {
    var where = {limit_purchased:limit_purchased};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照postage索引列出从n到n+size,size小于50，默认为20
  ,listPage_postage: function (offset,limit,postage,st,et,attributes) {
    var where = {postage:postage};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_postage 参数类型有误");
  }

  //按postage索引和时间范围统计created数
  ,count_postage: function(postage,st,et)
  {
    var where = {postage:postage};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['ecommerce'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.ecommerce.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.ecommerce.count({where: where});
  }
  
};
