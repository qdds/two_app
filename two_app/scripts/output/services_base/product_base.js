/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (product) {
    //判断是否为空
    if (product != null) {
      return models.product.create(product);
    } else {
      return Promise.reject("product对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (product,attributes) {
    if (typeof(product) === 'string') {
      return models.product.findOne({where: {productid: product},attributes:attributes});
    }
    else {
      if (product != null) {
        if (product.productid != null) {
          return models.product.findOne({where: {productid: product.productid},attributes:attributes});
        }
      }
    }
    return Promise.reject("product对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.product.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("product对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (product) {
    if (product != null) {
      if (product.productid != null) {
        return models.product.update(product, {where: {productid: product.productid}});
      }
    }
    return Promise.reject("product对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (product,obj) {
    if (product != null) {
      return models.product.update(product, {where: obj});
    }
    return Promise.reject("product对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (product) {
    if (typeof(product) === 'string') {
      return models.product.delete({where: {productid: product}});
    }
    else {
      if (product != null) {
        if (product.productid != null) {
          return models.product.delete({where: {productid: product.productid}});
        }
      }
    }
    return Promise.reject("product对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (product) {

      if (product != null) {
        return models.product.delete({where: product});
      }

    return Promise.reject("product对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.product.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.product.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['product'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.product.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.product.count({where: where});
  }

  
  //按照name索引列出从n到n+size,size小于50，默认为20
  ,listPage_name: function (offset,limit,name,st,et,attributes) {
    var where = {name:name};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['product'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.product.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_name 参数类型有误");
  }

  //按name索引和时间范围统计created数
  ,count_name: function(name,st,et)
  {
    var where = {name:name};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.product.count({where: where});
  }
  
  //按照category索引列出从n到n+size,size小于50，默认为20
  ,listPage_category: function (offset,limit,category,st,et,attributes) {
    var where = {category:category};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['product'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.product.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_category 参数类型有误");
  }

  //按category索引和时间范围统计created数
  ,count_category: function(category,st,et)
  {
    var where = {category:category};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.product.count({where: where});
  }
  
  //按照total_sales_volume索引列出从n到n+size,size小于50，默认为20
  ,listPage_total_sales_volume: function (offset,limit,total_sales_volume,st,et,attributes) {
    var where = {total_sales_volume:total_sales_volume};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['product'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.product.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_total_sales_volume 参数类型有误");
  }

  //按total_sales_volume索引和时间范围统计created数
  ,count_total_sales_volume: function(total_sales_volume,st,et)
  {
    var where = {total_sales_volume:total_sales_volume};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.product.count({where: where});
  }
  
  //按照total_sales索引列出从n到n+size,size小于50，默认为20
  ,listPage_total_sales: function (offset,limit,total_sales,st,et,attributes) {
    var where = {total_sales:total_sales};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['product'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.product.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_total_sales 参数类型有误");
  }

  //按total_sales索引和时间范围统计created数
  ,count_total_sales: function(total_sales,st,et)
  {
    var where = {total_sales:total_sales};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.product.count({where: where});
  }
  
  //按照postage索引列出从n到n+size,size小于50，默认为20
  ,listPage_postage: function (offset,limit,postage,st,et,attributes) {
    var where = {postage:postage};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['product'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.product.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_postage 参数类型有误");
  }

  //按postage索引和时间范围统计created数
  ,count_postage: function(postage,st,et)
  {
    var where = {postage:postage};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.product.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['product'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.product.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.product.count({where: where});
  }
  
};
