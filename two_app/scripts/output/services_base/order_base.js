/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (order) {
    //判断是否为空
    if (order != null) {
      return models.order.create(order);
    } else {
      return Promise.reject("order对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (order,attributes) {
    if (typeof(order) === 'string') {
      return models.order.findOne({where: {orderid: order},attributes:attributes});
    }
    else {
      if (order != null) {
        if (order.orderid != null) {
          return models.order.findOne({where: {orderid: order.orderid},attributes:attributes});
        }
      }
    }
    return Promise.reject("order对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.order.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("order对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (order) {
    if (order != null) {
      if (order.orderid != null) {
        return models.order.update(order, {where: {orderid: order.orderid}});
      }
    }
    return Promise.reject("order对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (order,obj) {
    if (order != null) {
      return models.order.update(order, {where: obj});
    }
    return Promise.reject("order对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (order) {
    if (typeof(order) === 'string') {
      return models.order.delete({where: {orderid: order}});
    }
    else {
      if (order != null) {
        if (order.orderid != null) {
          return models.order.delete({where: {orderid: order.orderid}});
        }
      }
    }
    return Promise.reject("order对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (order) {

      if (order != null) {
        return models.order.delete({where: order});
      }

    return Promise.reject("order对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.order.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.order.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['order'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.order.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.order.count({where: where});
  }

  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['order'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.order.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.order.count({where: where});
  }
  
  //按照userid索引列出从n到n+size,size小于50，默认为20
  ,listPage_userid: function (offset,limit,userid,st,et,attributes) {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['order'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.order.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_userid 参数类型有误");
  }

  //按userid索引和时间范围统计created数
  ,count_userid: function(userid,st,et)
  {
    var where = {userid:userid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.order.count({where: where});
  }
  
  //按照shelvesid索引列出从n到n+size,size小于50，默认为20
  ,listPage_shelvesid: function (offset,limit,shelvesid,st,et,attributes) {
    var where = {shelvesid:shelvesid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['order'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.order.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_shelvesid 参数类型有误");
  }

  //按shelvesid索引和时间范围统计created数
  ,count_shelvesid: function(shelvesid,st,et)
  {
    var where = {shelvesid:shelvesid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.order.count({where: where});
  }
  
  //按照productid索引列出从n到n+size,size小于50，默认为20
  ,listPage_productid: function (offset,limit,productid,st,et,attributes) {
    var where = {productid:productid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['order'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.order.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_productid 参数类型有误");
  }

  //按productid索引和时间范围统计created数
  ,count_productid: function(productid,st,et)
  {
    var where = {productid:productid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.order.count({where: where});
  }
  
  //按照couponid索引列出从n到n+size,size小于50，默认为20
  ,listPage_couponid: function (offset,limit,couponid,st,et,attributes) {
    var where = {couponid:couponid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['order'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.order.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_couponid 参数类型有误");
  }

  //按couponid索引和时间范围统计created数
  ,count_couponid: function(couponid,st,et)
  {
    var where = {couponid:couponid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.order.count({where: where});
  }
  
  //按照beauticianid索引列出从n到n+size,size小于50，默认为20
  ,listPage_beauticianid: function (offset,limit,beauticianid,st,et,attributes) {
    var where = {beauticianid:beauticianid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['order'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.order.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_beauticianid 参数类型有误");
  }

  //按beauticianid索引和时间范围统计created数
  ,count_beauticianid: function(beauticianid,st,et)
  {
    var where = {beauticianid:beauticianid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.order.count({where: where});
  }
  
};
