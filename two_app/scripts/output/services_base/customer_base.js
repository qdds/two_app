/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (customer) {
    //判断是否为空
    if (customer != null) {
      return models.customer.create(customer);
    } else {
      return Promise.reject("customer对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (customer,attributes) {
    if (typeof(customer) === 'string') {
      return models.customer.findOne({where: {customerid: customer},attributes:attributes});
    }
    else {
      if (customer != null) {
        if (customer.customerid != null) {
          return models.customer.findOne({where: {customerid: customer.customerid},attributes:attributes});
        }
      }
    }
    return Promise.reject("customer对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.customer.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("customer对象不能为空");
  }

  
  ,findOne_mobile: function (mobile,attributes) {
    if (mobile) {
      return models.customer.findOne({where: {mobile:mobile},attributes:attributes});
    }
    else {
      return Promise.reject("mobile不能为空");
    }
  }
  
  ,findOne_qr_code: function (qr_code,attributes) {
    if (qr_code) {
      return models.customer.findOne({where: {qr_code:qr_code},attributes:attributes});
    }
    else {
      return Promise.reject("qr_code不能为空");
    }
  }
  

  //根据主键更新记录
  ,update: function (customer) {
    if (customer != null) {
      if (customer.customerid != null) {
        return models.customer.update(customer, {where: {customerid: customer.customerid}});
      }
    }
    return Promise.reject("customer对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (customer,obj) {
    if (customer != null) {
      return models.customer.update(customer, {where: obj});
    }
    return Promise.reject("customer对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (customer) {
    if (typeof(customer) === 'string') {
      return models.customer.delete({where: {customerid: customer}});
    }
    else {
      if (customer != null) {
        if (customer.customerid != null) {
          return models.customer.delete({where: {customerid: customer.customerid}});
        }
      }
    }
    return Promise.reject("customer对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (customer) {

      if (customer != null) {
        return models.customer.delete({where: customer});
      }

    return Promise.reject("customer对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.customer.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.customer.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['customer'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.customer.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.customer.count({where: where});
  }

  
  //按照chain_group索引列出从n到n+size,size小于50，默认为20
  ,listPage_chain_group: function (offset,limit,chain_group,st,et,attributes) {
    var where = {chain_group:chain_group};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['customer'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.customer.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_chain_group 参数类型有误");
  }

  //按chain_group索引和时间范围统计created数
  ,count_chain_group: function(chain_group,st,et)
  {
    var where = {chain_group:chain_group};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.customer.count({where: where});
  }
  
  //按照chain_store索引列出从n到n+size,size小于50，默认为20
  ,listPage_chain_store: function (offset,limit,chain_store,st,et,attributes) {
    var where = {chain_store:chain_store};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['customer'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.customer.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_chain_store 参数类型有误");
  }

  //按chain_store索引和时间范围统计created数
  ,count_chain_store: function(chain_store,st,et)
  {
    var where = {chain_store:chain_store};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.customer.count({where: where});
  }
  
  //按照account_status索引列出从n到n+size,size小于50，默认为20
  ,listPage_account_status: function (offset,limit,account_status,st,et,attributes) {
    var where = {account_status:account_status};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['customer'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.customer.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_account_status 参数类型有误");
  }

  //按account_status索引和时间范围统计created数
  ,count_account_status: function(account_status,st,et)
  {
    var where = {account_status:account_status};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.customer.count({where: where});
  }
  
  //按照salesmanid索引列出从n到n+size,size小于50，默认为20
  ,listPage_salesmanid: function (offset,limit,salesmanid,st,et,attributes) {
    var where = {salesmanid:salesmanid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['customer'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.customer.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_salesmanid 参数类型有误");
  }

  //按salesmanid索引和时间范围统计created数
  ,count_salesmanid: function(salesmanid,st,et)
  {
    var where = {salesmanid:salesmanid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.customer.count({where: where});
  }
  
  //按照salesmanid索引列出从n到n+size,size小于50，默认为20
  ,listPage_salesmanid: function (offset,limit,salesmanid,st,et,attributes) {
    var where = {salesmanid:salesmanid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['customer'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.customer.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_salesmanid 参数类型有误");
  }

  //按salesmanid索引和时间范围统计created数
  ,count_salesmanid: function(salesmanid,st,et)
  {
    var where = {salesmanid:salesmanid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.customer.count({where: where});
  }
  
};
