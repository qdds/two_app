/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (service) {
    //判断是否为空
    if (service != null) {
      return models.service.create(service);
    } else {
      return Promise.reject("service对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (service,attributes) {
    if (typeof(service) === 'string') {
      return models.service.findOne({where: {serviceid: service},attributes:attributes});
    }
    else {
      if (service != null) {
        if (service.serviceid != null) {
          return models.service.findOne({where: {serviceid: service.serviceid},attributes:attributes});
        }
      }
    }
    return Promise.reject("service对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.service.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("service对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (service) {
    if (service != null) {
      if (service.serviceid != null) {
        return models.service.update(service, {where: {serviceid: service.serviceid}});
      }
    }
    return Promise.reject("service对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (service,obj) {
    if (service != null) {
      return models.service.update(service, {where: obj});
    }
    return Promise.reject("service对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (service) {
    if (typeof(service) === 'string') {
      return models.service.delete({where: {serviceid: service}});
    }
    else {
      if (service != null) {
        if (service.serviceid != null) {
          return models.service.delete({where: {serviceid: service.serviceid}});
        }
      }
    }
    return Promise.reject("service对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (service) {

      if (service != null) {
        return models.service.delete({where: service});
      }

    return Promise.reject("service对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.service.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.service.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.service.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.service.count({where: where});
  }

  
};
