/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (marketing) {
    //判断是否为空
    if (marketing != null) {
      return models.marketing.create(marketing);
    } else {
      return Promise.reject("marketing对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (marketing,attributes) {
    if (typeof(marketing) === 'string') {
      return models.marketing.findOne({where: {salesmanid: marketing},attributes:attributes});
    }
    else {
      if (marketing != null) {
        if (marketing.salesmanid != null) {
          return models.marketing.findOne({where: {salesmanid: marketing.salesmanid},attributes:attributes});
        }
      }
    }
    return Promise.reject("marketing对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.marketing.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("marketing对象不能为空");
  }

  
  ,findOne_account: function (account,attributes) {
    if (account) {
      return models.marketing.findOne({where: {account:account},attributes:attributes});
    }
    else {
      return Promise.reject("account不能为空");
    }
  }
  
  ,findOne_mobile: function (mobile,attributes) {
    if (mobile) {
      return models.marketing.findOne({where: {mobile:mobile},attributes:attributes});
    }
    else {
      return Promise.reject("mobile不能为空");
    }
  }
  

  //根据主键更新记录
  ,update: function (marketing) {
    if (marketing != null) {
      if (marketing.salesmanid != null) {
        return models.marketing.update(marketing, {where: {salesmanid: marketing.salesmanid}});
      }
    }
    return Promise.reject("marketing对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (marketing,obj) {
    if (marketing != null) {
      return models.marketing.update(marketing, {where: obj});
    }
    return Promise.reject("marketing对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (marketing) {
    if (typeof(marketing) === 'string') {
      return models.marketing.delete({where: {salesmanid: marketing}});
    }
    else {
      if (marketing != null) {
        if (marketing.salesmanid != null) {
          return models.marketing.delete({where: {salesmanid: marketing.salesmanid}});
        }
      }
    }
    return Promise.reject("marketing对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (marketing) {

      if (marketing != null) {
        return models.marketing.delete({where: marketing});
      }

    return Promise.reject("marketing对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.marketing.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.marketing.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['marketing'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.marketing.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.marketing.count({where: where});
  }

  
  //按照level索引列出从n到n+size,size小于50，默认为20
  ,listPage_level: function (offset,limit,level,st,et,attributes) {
    var where = {level:level};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['marketing'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.marketing.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_level 参数类型有误");
  }

  //按level索引和时间范围统计created数
  ,count_level: function(level,st,et)
  {
    var where = {level:level};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.marketing.count({where: where});
  }
  
  //按照mobile索引列出从n到n+size,size小于50，默认为20
  ,listPage_mobile: function (offset,limit,mobile,st,et,attributes) {
    var where = {mobile:mobile};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['marketing'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.marketing.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_mobile 参数类型有误");
  }

  //按mobile索引和时间范围统计created数
  ,count_mobile: function(mobile,st,et)
  {
    var where = {mobile:mobile};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.marketing.count({where: where});
  }
  
  //按照account索引列出从n到n+size,size小于50，默认为20
  ,listPage_account: function (offset,limit,account,st,et,attributes) {
    var where = {account:account};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['marketing'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.marketing.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_account 参数类型有误");
  }

  //按account索引和时间范围统计created数
  ,count_account: function(account,st,et)
  {
    var where = {account:account};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.marketing.count({where: where});
  }
  
  //按照password索引列出从n到n+size,size小于50，默认为20
  ,listPage_password: function (offset,limit,password,st,et,attributes) {
    var where = {password:password};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['marketing'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.marketing.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_password 参数类型有误");
  }

  //按password索引和时间范围统计created数
  ,count_password: function(password,st,et)
  {
    var where = {password:password};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.marketing.count({where: where});
  }
  
};
