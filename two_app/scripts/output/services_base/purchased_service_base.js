/**
 * Created by Wonderchief on 2017/1/6.
 * Legle co,.ltd.
 * This is a auto-generated code file.
 * 版权所有：广州聆歌信息科技有限公司
 */
var models = require('../models');
var Promise = require('bluebird');
var Moment = require('moment');

module.exports = {
  //创建新的记录
  create:function (purchased_service) {
    //判断是否为空
    if (purchased_service != null) {
      return models.purchased_service.create(purchased_service);
    } else {
      return Promise.reject("purchased_service对象不能为空");
    }
  }

  //根据主键查找一条记录
  ,findOne: function (purchased_service,attributes) {
    if (typeof(purchased_service) === 'string') {
      return models.purchased_service.findOne({where: {id: purchased_service},attributes:attributes});
    }
    else {
      if (purchased_service != null) {
        if (purchased_service.id != null) {
          return models.purchased_service.findOne({where: {id: purchased_service.id},attributes:attributes});
        }
      }
    }
    return Promise.reject("purchased_service对象不能为空");
  }

  //根据对象查找一条记录
  ,findOne_obj:function(_obj,attributes){
    if (_obj){
      return models.purchased_service.findOne({where:_obj,attributes:attributes});
    }
    return Promise.reject("purchased_service对象不能为空");
  }

  

  //根据主键更新记录
  ,update: function (purchased_service) {
    if (purchased_service != null) {
      if (purchased_service.id != null) {
        return models.purchased_service.update(purchased_service, {where: {id: purchased_service.id}});
      }
    }
    return Promise.reject("purchased_service对象不能为空");
  }

  //根据对象更新记录
  ,update_obj: function (purchased_service,obj) {
    if (purchased_service != null) {
      return models.purchased_service.update(purchased_service, {where: obj});
    }
    return Promise.reject("purchased_service对象不能为空");
  }

  //根据主键删除记录
  ,delete: function (purchased_service) {
    if (typeof(purchased_service) === 'string') {
      return models.purchased_service.delete({where: {id: purchased_service}});
    }
    else {
      if (purchased_service != null) {
        if (purchased_service.id != null) {
          return models.purchased_service.delete({where: {id: purchased_service.id}});
        }
      }
    }
    return Promise.reject("purchased_service对象不能为空");
  }

  //根据对象删除记录
    ,delete_obj: function (purchased_service) {

      if (purchased_service != null) {
        return models.purchased_service.delete({where: purchased_service});
      }

    return Promise.reject("purchased_service对象不能为空");
  }

  //列出前1000条记录
  ,list: function (limit) {
    if(typeof(limit)==='number')
    {
      if(limit>1000)
      {
        limit=1000;
      }
      return models.purchased_service.findAll({limit:limit});
    }else {
      if(limit==null)
          return models.purchased_service.findAll({limit:1000});
    }

    return Promise.reject("list 参数类型有误");
  }

  //根据日期范围内列出指定字段和，从n到n+size，size小于50，默认为20，没有日期范围则列出最新
  ,listPage: function (offset,limit,st,et,attributes) {
    var where = {};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['purchased_service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.purchased_service.findAll({where: where, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage 参数类型有误");
  }


  //按时间范围统计created数
  ,count: function(st,et)
  {
    var where={};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.purchased_service.count({where: where});
  }

  
  //按照serviceid索引列出从n到n+size,size小于50，默认为20
  ,listPage_serviceid: function (offset,limit,serviceid,st,et,attributes) {
    var where = {serviceid:serviceid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['purchased_service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.purchased_service.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_serviceid 参数类型有误");
  }

  //按serviceid索引和时间范围统计created数
  ,count_serviceid: function(serviceid,st,et)
  {
    var where = {serviceid:serviceid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.purchased_service.count({where: where});
  }
  
  //按照salesmanid索引列出从n到n+size,size小于50，默认为20
  ,listPage_salesmanid: function (offset,limit,salesmanid,st,et,attributes) {
    var where = {salesmanid:salesmanid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['purchased_service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.purchased_service.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_salesmanid 参数类型有误");
  }

  //按salesmanid索引和时间范围统计created数
  ,count_salesmanid: function(salesmanid,st,et)
  {
    var where = {salesmanid:salesmanid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.purchased_service.count({where: where});
  }
  
  //按照buy_datetime索引列出从n到n+size,size小于50，默认为20
  ,listPage_buy_datetime: function (offset,limit,buy_datetime,st,et,attributes) {
    var where = {buy_datetime:buy_datetime};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['purchased_service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.purchased_service.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_buy_datetime 参数类型有误");
  }

  //按buy_datetime索引和时间范围统计created数
  ,count_buy_datetime: function(buy_datetime,st,et)
  {
    var where = {buy_datetime:buy_datetime};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.purchased_service.count({where: where});
  }
  
  //按照finish_datetime索引列出从n到n+size,size小于50，默认为20
  ,listPage_finish_datetime: function (offset,limit,finish_datetime,st,et,attributes) {
    var where = {finish_datetime:finish_datetime};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['purchased_service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.purchased_service.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_finish_datetime 参数类型有误");
  }

  //按finish_datetime索引和时间范围统计created数
  ,count_finish_datetime: function(finish_datetime,st,et)
  {
    var where = {finish_datetime:finish_datetime};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.purchased_service.count({where: where});
  }
  
  //按照payment_status索引列出从n到n+size,size小于50，默认为20
  ,listPage_payment_status: function (offset,limit,payment_status,st,et,attributes) {
    var where = {payment_status:payment_status};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['purchased_service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.purchased_service.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_payment_status 参数类型有误");
  }

  //按payment_status索引和时间范围统计created数
  ,count_payment_status: function(payment_status,st,et)
  {
    var where = {payment_status:payment_status};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.purchased_service.count({where: where});
  }
  
  //按照payments索引列出从n到n+size,size小于50，默认为20
  ,listPage_payments: function (offset,limit,payments,st,et,attributes) {
    var where = {payments:payments};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['purchased_service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.purchased_service.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_payments 参数类型有误");
  }

  //按payments索引和时间范围统计created数
  ,count_payments: function(payments,st,et)
  {
    var where = {payments:payments};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.purchased_service.count({where: where});
  }
  
  //按照serviceid索引列出从n到n+size,size小于50，默认为20
  ,listPage_serviceid: function (offset,limit,serviceid,st,et,attributes) {
    var where = {serviceid:serviceid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['purchased_service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.purchased_service.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_serviceid 参数类型有误");
  }

  //按serviceid索引和时间范围统计created数
  ,count_serviceid: function(serviceid,st,et)
  {
    var where = {serviceid:serviceid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.purchased_service.count({where: where});
  }
  
  //按照customerid索引列出从n到n+size,size小于50，默认为20
  ,listPage_customerid: function (offset,limit,customerid,st,et,attributes) {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['purchased_service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.purchased_service.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_customerid 参数类型有误");
  }

  //按customerid索引和时间范围统计created数
  ,count_customerid: function(customerid,st,et)
  {
    var where = {customerid:customerid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.purchased_service.count({where: where});
  }
  
  //按照salesmanid索引列出从n到n+size,size小于50，默认为20
  ,listPage_salesmanid: function (offset,limit,salesmanid,st,et,attributes) {
    var where = {salesmanid:salesmanid};
    if (st != null || et != null) {
      where.createdAt = {
          $between: [st, et]
      }
    }
    if (typeof(offset) === 'number') {
      if (limit > 50) {
        limit = 50;
      }
    }
    else {
      limit = 20;
    }
    if (limit == null)limit = 20;
    if (typeof(offset) === 'number') {
      return models['purchased_service'].findAll({where: where,attributes:attributes, limit: limit, offset: offset,order: 'createdAt DESC'});
    } else {
      if (offset == null)
        return models.purchased_service.findAll({where: where,attributes:attributes, limit: limit,order: 'createdAt DESC'});
    }

    return Promise.reject("listPage_salesmanid 参数类型有误");
  }

  //按salesmanid索引和时间范围统计created数
  ,count_salesmanid: function(salesmanid,st,et)
  {
    var where = {salesmanid:salesmanid};
    if (st != null || et != null) {
      where.createdAt = {
        $between: [st, et]
      }
    }
    return models.purchased_service.count({where: where});
  }
  
};
